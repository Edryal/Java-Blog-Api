# Java Blog API
An API written in Java using Spring Boot.

### About project:
* Theme: Blog Website
* Frameworks: Spring Boot & Hibernate
* Database: MongoDB

### Swagger-UI:
#### http://localhost:8080/swagger-ui.html
