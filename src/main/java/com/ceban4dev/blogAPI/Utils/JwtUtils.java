package com.ceban4dev.blogAPI.Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.Date;

public class JwtUtils {

    private static final String SECRET_KEY = "S3cr3tK3y#F0rJWT!4rp";
    private static final long EXPIRATION_TIME = 3600000;
    private static final Key KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    public static String generateAccessToken(String username) {
        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + EXPIRATION_TIME);
        //System.out.println();



        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(KEY)
                .compact();


    }

    public static String getUsernameFromAccessToken(String accessToken) {
        try {

            Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(KEY).build().parseClaimsJws(accessToken);
            Claims claims = claimsJws.getBody();
            return claims.getSubject();
        } catch (Exception e) {
            return null;
        }
    }
    public static boolean validateToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(KEY).build().parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
