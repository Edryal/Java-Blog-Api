package com.ceban4dev.blogAPI.Config;

import com.ceban4dev.blogAPI.Utils.JwtAuthorizationInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new JwtAuthorizationInterceptor())
                .addPathPatterns("/protected/**");
    }
}
