package com.ceban4dev.blogAPI.Site.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/protected")
public class ProtectedResourceController {

    @GetMapping("/data")
    public String getProtectedData() {
        return "Coming soon!";
    }
}