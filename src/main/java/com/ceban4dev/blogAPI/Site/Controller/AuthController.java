package com.ceban4dev.blogAPI.Site.Controller;


import com.ceban4dev.blogAPI.Site.Service.AuthService;
import com.ceban4dev.blogAPI.Utils.JwtUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AuthController {
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody Map<String, String> credentials) {
        String username = credentials.get("username");
        String password = credentials.get("password");
        try {


            if (AuthService.authenticate(username, password)) {

                String accessToken = JwtUtils.generateAccessToken(username);
                //System.out.print(accessToken);

                return ResponseEntity.ok(accessToken);
            } else {

                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials");
            }
        }catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error: " + e.getMessage());

        }
    }

    @GetMapping("/secure/resource")
    public ResponseEntity<String> getSecureResource(@RequestHeader("Authorization") String authorizationHeader) {
        String accessToken = authorizationHeader.replace("Bearer ", "");
        String username = JwtUtils.getUsernameFromAccessToken(accessToken);

        if (username != null) {

            String resourceData = "This is a secure resource for user: " + username;
            return ResponseEntity.ok(resourceData);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid or missing AccessToken");
        }
    }
}
