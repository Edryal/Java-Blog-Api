package com.ceban4dev.blogAPI.Site.DTO;

public record UserDTO (
    String id,
    String name,
    String email,
    String username,
    String password
) {

}
