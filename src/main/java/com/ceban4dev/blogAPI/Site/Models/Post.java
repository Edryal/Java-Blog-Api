package com.ceban4dev.blogAPI.Site.Models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "Posts")
@TypeAlias("")
public class Post {

    @Id
    private String id;

    @Field("text")
    private String text;

    @Field("username")
    private String username;

    public Post() {

    }

    public Post(String id, String text, String Username) {
        this.id = id;
        this.text = text;
        this.username = Username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
