package com.ceban4dev.blogAPI.Site.Models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "Comments")
@TypeAlias("")
public class Comment {

    @Id
    private String id;

    @Field("text")
    private String text;

    @Field("username")
    private String username;

    @Field("postId")
    private String postId;

    @Field("dataCreate")
    private String dataCreate;

    public Comment() {

    }

    public Comment(String id, String text, String username, String postId, String dataCreate) {
        this.id = id;
        this.text = text;
        this.username = username;
        this.postId = postId;
        this.dataCreate = dataCreate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

    public void setDataCreate(String dataCreate) {
        this.dataCreate = dataCreate;
    }

    public String getDataCreate() {
        return dataCreate;
    }
}
