package com.ceban4dev.blogAPI.Site.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.ceban4dev.blogAPI.Site.Models.Comment;

public interface CommentRepository extends MongoRepository<Comment, String> {

}
