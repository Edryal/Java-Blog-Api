package com.ceban4dev.blogAPI.Site.Repository;

import com.ceban4dev.blogAPI.Site.Models.Post;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PostRepository extends MongoRepository<Post, String> {

}
