package com.ceban4dev.blogAPI.Site.Repository;

import com.ceban4dev.blogAPI.Site.Models.User;
import org.springframework.data.mongodb.repository.DeleteQuery;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
    @DeleteQuery
    void deleteUserById(String id);
}
