package com.ceban4dev.blogAPI.Site.Service;

import java.util.HashMap;
import java.util.Map;

public class AuthService {
    private static final Map<String,String> users = new HashMap<>();
    static{
        users.put("Dexpomar","admin");
        users.put("Liebe","admin");
    }

    public static boolean authenticate(String username, String password) {
        String storedPassword = users.get(username);
        return storedPassword!= null && storedPassword.equals(password);
    }
}
