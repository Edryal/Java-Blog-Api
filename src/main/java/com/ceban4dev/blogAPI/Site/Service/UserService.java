package com.ceban4dev.blogAPI.Site.Service;

import com.ceban4dev.blogAPI.Site.DTO.UserDTO;
import com.ceban4dev.blogAPI.Site.DTO.UserDTOMapper;
import com.ceban4dev.blogAPI.Site.Models.User;
import com.ceban4dev.blogAPI.Site.Repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserDTOMapper userDTOMapper;

    @Autowired
    public UserService(UserRepository userRepository, UserDTOMapper userDTOMapper) {
        this.userRepository = userRepository;
        this.userDTOMapper = userDTOMapper;
    }

    public List<UserDTO> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(userDTOMapper)
                .collect(Collectors.toList());
    }

    public UserDTO getUserById(String id) {
        return userRepository.findById(id)
                .map(userDTOMapper)
                .orElseThrow(() -> new NoSuchElementException(
                        "User with id [%s] not found!".formatted(id)
                ));
    }

    public void createUser(User newUser) {
        User user = new User(
                newUser.getId(),
                newUser.getName(),
                newUser.getEmail(),
                newUser.getTypeOfUser(),
                newUser.getUsername(),
                newUser.getPassword()
        );
        userRepository.save(user);
    }

    public void updateUser(String id, User newUser) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(
                "User with id [%s] not found!".formatted(id)
        ));
        BeanUtils.copyProperties(user, newUser);
        userRepository.save(user);
    }

    public void deleteUser(String id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            ResponseEntity.internalServerError().build();
            return;
        }
        userRepository.deleteUserById(id);
        ResponseEntity.ok().build();
    }
}
